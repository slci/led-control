package mar.gluchowska.ledcontrol.tabs;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import mar.gluchowska.ledcontrol.BTLedDevice;
import mar.gluchowska.ledcontrol.ColorPicker;
import mar.gluchowska.ledcontrol.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ColorPickerFragment extends Fragment implements View.OnClickListener {

    private ColorPicker mColorPickerView;

    public ColorPickerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_color_picker, container, false);

        view.findViewById(R.id.btn_white).setOnClickListener(this);
        view.findViewById(R.id.btn_red).setOnClickListener(this);
        view.findViewById(R.id.btn_green).setOnClickListener(this);
        view.findViewById(R.id.btn_blue).setOnClickListener(this);

        mColorPickerView = view.findViewById(R.id.colorPicker);

        mColorPickerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == (MotionEvent.ACTION_UP)) {

                    int color = mColorPickerView.getColor();
                    BTLedDevice.getInstance().sendRgbString(getContext(), Color.red(color), Color.green(color), Color.blue(color));

                    return true;

                } else {

                    return false;
                }
            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_white:
                mColorPickerView.setColor(255, 255, 255);
                BTLedDevice.getInstance().sendRgbString(getContext(), 255, 255, 255);
                break;
            case R.id.btn_red:
                mColorPickerView.setColor(255, 0, 0);
                BTLedDevice.getInstance().sendRgbString(getContext(), 255, 0, 0);
                break;
            case R.id.btn_green:
                mColorPickerView.setColor(0, 255, 0);
                BTLedDevice.getInstance().sendRgbString(getContext(), 0, 255, 0);
                break;
            case R.id.btn_blue:
                mColorPickerView.setColor(0, 0, 255);
                BTLedDevice.getInstance().sendRgbString(getContext(), 0, 0, 255);
                break;

        }
    }
}
